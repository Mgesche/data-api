SELECT SYS_BatchId, Rank, PackageName, DtLaunch
INTO #LastBatchs
FROM (
SELECT PackageFolder, PackageProject, PackageName, SYS_BatchId, DtLaunch, RANK() OVER(PARTITION BY PackageFolder, PackageProject, PackageName ORDER BY DtLaunch DESC) AS RANK
FROM ETLCore.biml.Batchs
WHERE PackageName LIKE 'FIN_MDS%'
) RES
WHERE RANK < 11

SELECT PackageName, EntityName, [VersionName],  Rank + ' (' + CONVERT(VARCHAR, DtLaunch, 104) + ' ' + CONVERT(VARCHAR, DtLaunch, 114) + ')' AS Launch, NbLines FROM (
SELECT 'Account' AS EntityName
	  ,BAT.Rank
    ,BAT.DtLaunch
	  ,BAT.PackageName
	  ,[VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Account] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Authorization_of_Expenditure' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Authorization_of_Expenditure] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Business_Unit' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Business_Unit] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Cost_Centre' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Cost_Centre] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Currency' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Currency] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Customer' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Customer] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Customer_Category' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Customer_Category] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Fixed_Asset' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Fixed_Asset] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'General_Objective' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[General_Objective] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Objective' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Objective] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Organizational_Unit' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Organizational_Unit] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Project' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Project] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Specific_Objective' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Specific_Objective] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
UNION
SELECT 'Unit_Of_Measure' AS EntityName,BAT.Rank,BAT.DtLaunch,BAT.PackageName, [VersionName]
      ,COUNT(*) AS NbLines
FROM [FIN_ODS].[mds_exp].[Unit_Of_Measure] MDS
JOIN #LastBatchs BAT
  ON MDS.SYS_BatchId = BAT.SYS_BatchId
GROUP BY [VersionName], BAT.Rank,BAT.DtLaunch,BAT.PackageName
) RES
ORDER BY PackageName, EntityName, VersionName, Rank

DROP TABLE #LastBatchs