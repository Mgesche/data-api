SELECT 'UPDATE biml.Transfer SET IsToPush = 0, IsToRefresh = 0 WHERE SourceAppName = '' + SourceAppName + '' AND TargetAppName = '' + TargetAppName + '' AND SourceEntityName = '' + SourceEntityName + '' AND TargetEntityName = '' + TargetEntityName + ''' AS SQLCommand
FROM biml.Transfer
WHERE SourceAppName = 'PeopleSoft'
  AND TargetAppName = 'FIN_ODS'
  AND (IsToPush <> 1
   OR IsToRefresh <> 1)