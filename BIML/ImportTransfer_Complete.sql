SELECT [SourceAppName]
	  ,SRC.[SchemaName] AS SourceAppSchemaName
      ,SRC.[DomainName] AS SourceAppDomainName
      ,SRC.[AppType] AS SourceAppType
      ,SRC.[ConnectionStringExample] AS SourceAppConnectionStringExample
      ,SRC.[DBName] AS SourceAppDBName
      ,SRC.[LocalSchemaName] AS SourceAppLocalSchemaName
      ,SRC.[GlobalApplicationName] AS SourceAppGlobalApplicationName
      ,SRC.[Comments] AS SourceAppComments
      ,[SourceEntityName]
      ,[SourceFilterQuery]
      ,[SourceSelectQuery]
      ,[SourceConditionToExec]
      ,[TargetAppName]
	  ,TAR.[SchemaName] AS TargetAppSchemaName
      ,TAR.[DomainName] AS TargetAppDomainName
      ,TAR.[AppType] AS TargetAppType
      ,TAR.[ConnectionStringExample] AS TargetAppConnectionStringExample
      ,TAR.[DBName] AS TargetAppDBName
      ,TAR.[LocalSchemaName] AS TargetAppLocalSchemaName
      ,TAR.[GlobalApplicationName] AS TargetAppGlobalApplicationName
      ,TAR.[Comments] AS TargetAppComments
      ,[TargetEntityName]
      ,[LastPropagationKey]
      ,[IsToRefresh]
      ,[IsToPush]
      ,[Frequency]
      ,[TransferType]
FROM [ETLCore].[biml].[Transfer] TRA
JOIN [ETLCore].[biml].[Applications] SRC
  ON SRC.AppName = TRA.SourceAppName
JOIN [ETLCore].[biml].[Applications] TAR
  ON TAR.AppName = TRA.TargetAppName