DECLARE @TABLE_NAME VARCHAR(MAX)
SET @TABLE_NAME = '%TableName%'
DECLARE @TABLE_SCHEMA VARCHAR(MAX)
SET @TABLE_SCHEMA = '%SchemaName%'
DECLARE @Select VARCHAR(MAX)
SET @Select = ''

SELECT @Select = @Select + CASE WHEN DATA_TYPE IN ('VARCHAR', 'NVARCHAR') THEN 'RTRIM([' + COLUMN_NAME + ']) AS [' + COLUMN_NAME + ']' ELSE '[' + COLUMN_NAME + ']' END + ','
FROM %DB_NAME%.INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TABLE_NAME
  AND TABLE_SCHEMA = @TABLE_SCHEMA
  AND COLUMN_NAME NOT IN ('SYS_BatchId','SYS_LastModified','SYS_HashColumn')
ORDER BY ORDINAL_POSITION

SELECT 'UPDATE biml.Transfer SET SourceSelectQuery = ''' + LEFT(@Select, LEN(@Select)-1) + ''' WHERE SourceSelectQuery <> ''' + LEFT(@Select, LEN(@Select)-1) + ''' AND SourceAppName = ''%SourceAppName%'' AND TargetAppName = ''%TargetAppName%'' AND SourceEntityName = ''%SourceEntityName%'' AND TargetEntityName = ''%TargetEntityName%''' AS SelectQuery