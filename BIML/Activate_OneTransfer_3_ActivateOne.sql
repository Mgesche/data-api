UPDATE biml.Transfer
SET IsToPush = 1, IsToRefresh = 1
WHERE SourceAppName = 'PeopleSoft'
  AND TargetAppName = 'FIN_ODS'
  AND SourceEntityName = 'PS_SET_CNTRL_REC'
  AND TargetEntityName = 'PS_SET_CNTRL_REC'