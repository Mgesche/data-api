SELECT * FROM (
SELECT nomJob
  , nomEtapeJob
  , numeroEtapeJob
  , msgEtape
  ,dateExecutionEtape
  ,dureeExecutionEtape
  ,statutExecutionEtape
  ,RANK() OVER(PARTITION BY nomJob ORDER BY 
	CASE WHEN statutExecutionEtape = 'FAILED' THEN 1 ELSE 0 END DESC, 
	dateExecutionEtape DESC, 
	numeroEtapeJob ASC, 
	msgEtape DESC) AS RANK
FROM (
SELECT  nomJob
  , nomEtapeJob
  , numeroEtapeJob
  , msgEtape
  , CAST(STUFF(STUFF(dateExecutionEtape, 12, 0, ':'), 15, 0, ':') AS DATETIME) AS dateExecutionEtape
  , dureeExecutionEtape
  , statutExecutionEtape
  , Rank
FROM  
(  
  SELECT    J.name AS nomJob 
      , S.step_name AS nomEtapeJob 
      , S.step_id AS numeroEtapeJob 
      , ISNULL('Erreur : ' + M.description, H.message) AS msgEtape 
      , CAST(H.run_date AS CHAR(8)) + ' ' +  
      CASE LEN(CAST(H.run_time AS VARCHAR(6))) 
        WHEN 1 THEN '00000' + CAST(H.run_time AS CHAR(1))  
        WHEN 2 THEN '0000' + CAST(H.run_time AS CHAR(2))  
        WHEN 3 THEN '000' + CAST(H.run_time AS CHAR(3))  
        WHEN 4 THEN '00' + CAST(H.run_time AS CHAR(4))  
        WHEN 5 THEN '0' + CAST(H.run_time AS CHAR(5))  
        ELSE CAST(H.run_time AS CHAR(6))  
      END AS dateExecutionEtape 
      , CASE LEN(CAST(H.run_duration AS VARCHAR(10)))  
        WHEN 1 THEN '00:00:0' + CAST(H.run_duration AS CHAR(1))  
        WHEN 2 THEN '00:00:' + CAST(H.run_duration AS CHAR(2))  
        WHEN 3 THEN '00:0' + STUFF(CAST(H.run_duration AS CHAR(3)), 2, 0, ':')  
        WHEN 4 THEN '00:' + STUFF(CAST(H.run_duration AS CHAR(4)), 3, 0, ':')  
        WHEN 5 THEN '0' + STUFF(STUFF(CAST(H.run_duration AS CHAR(5)), 2, 0, ':'), 5, 0, ':')  
        WHEN 6 THEN STUFF(STUFF(CAST(H.run_duration AS CHAR(6)), 3, 0, ':'), 6, 0, ':')  
      END AS dureeExecutionEtape 
      , CASE H.run_status  
        WHEN 0 THEN 'FAILED' 
        WHEN 1 THEN 'SUCCESS' 
        WHEN 2 THEN 'RETRY' 
        WHEN 3 THEN 'CANCELED' 
        WHEN 4 THEN 'RUNNING' -- Pas fiable 
      END AS statutExecutionEtape,
      H.RANK AS Rank
  FROM msdb.dbo.sysjobs AS J   
  INNER JOIN  msdb.dbo.sysjobsteps AS S  
        ON J.job_id = S.job_id  
  LEFT JOIN (
      SELECT * FROM (
SELECT *, RANK() OVER(PARTITION BY job_id, step_id ORDER BY run_date DESC, run_time DESC) AS RANK
FROM msdb.dbo.sysjobhistory) RES
WHERE Rank = 1) AS H  
        ON S.job_id = H.job_id  
        AND S.step_id = H.step_id  
  LEFT JOIN  sys.sysmessages AS M  
        ON H.sql_message_id = M.error  
) AS JOB_CHARACTERISTICS (nomJob, nomEtapeJob, numeroEtapeJob, msgEtape, dateExecutionEtape, dureeExecutionEtape, statutExecutionEtape, Rank)
) RES
) RES
WHERE RANK = 1