SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

-- DECLARE @job_name sysname
DECLARE @job_id UNIQUEIDENTIFIER
DECLARE @job_owner sysname
DECLARE @JobCompletionStatus VARCHAR(255)
DECLARE @DtDebut DATETIME
DECLARE @DtFin DATETIME
DECLARE @Duration INT

SELECT @DtDebut = GETUTCDATE()

--Createing TEMP TABLE
CREATE TABLE #xp_results (
    job_id UNIQUEIDENTIFIER NOT NULL,
    last_run_date INT NOT NULL,
    last_run_time INT NOT NULL,
    next_run_date INT NOT NULL,
    next_run_time INT NOT NULL,
    next_run_schedule_id INT NOT NULL,
    requested_to_run INT NOT NULL, -- BOOL
    request_source INT NOT NULL,
    request_source_id sysname COLLATE database_default NULL,
    running INT NOT NULL, -- BOOL
    current_step INT NOT NULL,
    current_retry_attempt INT NOT NULL,
    job_state INT NOT NULL
)

SELECT @job_id = job_id FROM msdb.dbo.sysjobs
WHERE name = '%JobName%'

SELECT @job_owner = SUSER_SNAME()

INSERT INTO #xp_results
EXECUTE master.dbo.xp_sqlagent_enum_jobs 1, @job_owner, @job_id 

-- Start the job if the job is not running
IF NOT EXISTS(SELECT TOP 1 * FROM #xp_results WHERE running = 1)
EXEC msdb.dbo.sp_start_job @job_name = '%JobName%'

-- Give 2 sec for think time.
WAITFOR DELAY '00:00:02'

DELETE FROM #xp_results
INSERT INTO #xp_results
EXECUTE master.dbo.xp_sqlagent_enum_jobs 1, @job_owner, @job_id 

WHILE EXISTS(SELECT TOP 1 * FROM #xp_results WHERE running = 1)
BEGIN

WAITFOR DELAY '%WaitTime%'

-- Information 
raiserror('JOB IS RUNNING', 0, 1 ) WITH NOWAIT 

DELETE FROM #xp_results

INSERT INTO #xp_results
EXECUTE master.dbo.xp_sqlagent_enum_jobs 1, @job_owner, @job_id 

END

SELECT top 1 @JobCompletionStatus = CASE run_status
        WHEN 1 THEN 'Successful'
        WHEN 3 THEN 'Cancelled'
        ELSE 'Error'
    END
FROM msdb.dbo.sysjobhistory
WHERE job_id = @job_id
and step_id = 0
order by run_date desc, run_time desc

IF @JobCompletionStatus = 'Successful'
PRINT 'The job ran Successful' 
ELSE IF @JobCompletionStatus = 'Cancelled'
PRINT 'The job is Cancelled'
ELSE 
BEGIN
RAISERROR ('[ERROR]:%s job is either failed or not in good state. Please check',16, 1, '%JobName%') WITH LOG
END

SELECT @DtFin = GETUTCDATE()
SELECT @Duration = DATEDIFF(SECOND, @DtFin, @DtDebut)

SELECT  '%JobName%' AS JobName, @JobCompletionStatus AS JobCompletionStatus, 
        CONVERT(VARCHAR, @DtDebut, 104) + ' ' + CONVERT(VARCHAR, @DtDebut, 114) AS DtDebut, 
        CONVERT(VARCHAR, @DtFin, 104) + ' ' + CONVERT(VARCHAR, @DtFin, 114) AS DtFin,
        @Duration AS Duration