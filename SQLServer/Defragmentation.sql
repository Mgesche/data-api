SELECT
    FRA.object_id AS objectid,
	QUOTENAME(o.name) AS objectname,
	QUOTENAME(s.name) AS schemaname,
    FRA.index_id AS indexid,
	QUOTENAME(ind.name) AS indexname,
	ind.type_desc AS indextype,
    partition_number AS partitionnum,
	PAR.partitioncount AS partitioncount,
    avg_fragmentation_in_percent AS frag,
	CASE WHEN avg_fragmentation_in_percent > 10.0 THEN
        CASE WHEN avg_fragmentation_in_percent < 30.0
             THEN N'ALTER INDEX ' + QUOTENAME(ind.name) + N' ON ' + QUOTENAME(s.name) + N'.' + QUOTENAME(o.name) + N' REORGANIZE'
             ELSE N'ALTER INDEX ' + QUOTENAME(ind.name) + N' ON ' + QUOTENAME(s.name) + N'.' + QUOTENAME(o.name) + N' REBUILD'
	    END + CASE WHEN PAR.partitioncount > 1 THEN N' PARTITION=' + CAST(partition_number AS nvarchar(10)) ELSE '' END
    END AS SQLCommand
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED') FRA
JOIN sys.objects o
  ON o.object_id = FRA.object_id
JOIN sys.schemas s
  ON s.schema_id = o.schema_id
JOIN sys.indexes ind
  ON ind.index_id = FRA.index_id
 AND ind.object_id = FRA.object_id
JOIN (SELECT object_id, index_id, count(*) AS partitioncount
FROM sys.partitions
GROUP BY object_id, index_id) PAR
  ON FRA.object_id = PAR.object_id
 AND FRA.index_id = PAR.index_id
WHERE FRA.index_id > 0
  AND avg_fragmentation_in_percent > 10.0;