DECLARE @TableName VARCHAR(255)
DECLARE @SchemaName VARCHAR(255)
DECLARE @TableName_History VARCHAR(255)
DECLARE @SchemaName_History VARCHAR(255)

CREATE TABLE #Result(
	TableName VARCHAR(255),
	SchemaName VARCHAR(255),
	TableName_History VARCHAR(255),
	SchemaName_History VARCHAR(255),
	SelectColumnAGG VARCHAR(MAX),
	GroupByColumn VARCHAR(MAX),
	SelectColumn VARCHAR(MAX)
)

DECLARE tab_cursor CURSOR FOR
SELECT OBJ.name AS TableName, SCE.name AS SchemaName, HIS.name AS TableName_History, SCE_HIS.name AS SchemaName_History
FROM sys.tables TAB
JOIN sys.objects OBJ
  ON OBJ.object_id = TAB.object_id
JOIN sys.schemas SCE
  ON OBJ.schema_id = SCE.schema_id
JOIN sys.objects HIS
  ON HIS.object_id = TAB.history_table_id
JOIN sys.schemas SCE_HIS
  ON HIS.schema_id = SCE_HIS.schema_id
WHERE TAB.history_table_id IS NOT NULL

OPEN tab_cursor
FETCH NEXT FROM tab_cursor INTO @TableName, @SchemaName, @TableName_History, @SchemaName_History

WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @SelectColumnAGG VARCHAR(MAX)
	SET @SelectColumnAGG = ''
	DECLARE @GroupByColumn VARCHAR(MAX)
	SET @GroupByColumn = ''
	DECLARE @SelectColumn VARCHAR(MAX)
	SET @SelectColumn = ''

	SELECT	@SelectColumnAGG = @SelectColumnAGG + CASE WHEN COLUMN_NAME IN ('SYS_BatchId', 'SYS_LastModified', 'SysStartTime') THEN 'MIN([' + COLUMN_NAME + ']) AS [' + COLUMN_NAME + ']' ELSE CASE WHEN COLUMN_NAME = 'SysEndTime' THEN 'MAX([' + COLUMN_NAME + ']) AS [' + COLUMN_NAME + ']' ELSE CASE WHEN data_type = 'xml' THEN 'CAST([' + COLUMN_NAME + '] AS NVARCHAR(MAX)) AS [' + COLUMN_NAME + ']' ELSE '[' + COLUMN_NAME + ']' END END END + ',',
			@GroupByColumn = @GroupByColumn + CASE WHEN COLUMN_NAME IN ('SYS_BatchId', 'SYS_LastModified', 'SysStartTime', 'SysEndTime') THEN '' ELSE CASE WHEN data_type = 'xml' THEN 'CAST([' + COLUMN_NAME + '] AS NVARCHAR(MAX)),' ELSE '[' + COLUMN_NAME + '],' END END,
			@SelectColumn = @SelectColumn + '[' + COLUMN_NAME + '],'
	FROM INFORMATION_SCHEMA.COLUMNS COL
	WHERE TABLE_SCHEMA = @SchemaName
	  AND TABLE_NAME = @TableName
	ORDER BY ORDINAL_POSITION

	INSERT INTO #Result
	SELECT	@TableName,
			@SchemaName,
			@TableName_History,
			@SchemaName_History,
			LEFT(@SelectColumnAGG, LEN(@SelectColumnAGG)-1),
			LEFT(@GroupByColumn, LEN(@GroupByColumn)-1),
			LEFT(@SelectColumn, LEN(@SelectColumn)-1)

	FETCH NEXT FROM tab_cursor INTO @TableName, @SchemaName, @TableName_History, @SchemaName_History
END

CLOSE tab_cursor
DEALLOCATE tab_cursor

SELECT *
FROM #Result

DROP TABLE #Result