DECLARE @Query NVARCHAR(MAX)
DECLARE @SchemaName VARCHAR(255)
DECLARE @TableName VARCHAR(255)
DECLARE @ColumnName VARCHAR(255)
DECLARE @ColumnCount INT

SET @SchemaName = REPLACE(REPLACE('%SchemaName%', '[', ''), ']', '')
SET @TableName = REPLACE(REPLACE('%TableName%', '[', ''), ']', '')

SET @Query = 'CREATE TABLE #Results (ColumnName VARCHAR(255), ColumnCount INT);'

-- Get Candidates column
DECLARE db_cursor CURSOR FOR
SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TableName
  AND TABLE_SCHEMA = @SchemaName
  AND COLUMN_NAME NOT IN ('SYS_BatchId', 'SYS_LastModified', 'SYS_HashColumn')
  AND (RIGHT(COLUMN_NAME, 3) = '_ID'
   OR RIGHT(COLUMN_NAME, 3) = '_CD'
   OR RIGHT(COLUMN_NAME, 5) = '_CODE'
   OR RIGHT(COLUMN_NAME, 5) = '_UNIT')
   
OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @ColumnName

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @Query = @Query + 'INSERT INTO #Results SELECT ''[' + @ColumnName + ']'', COUNT(DISTINCT [' + @ColumnName + ']) FROM [' + @SchemaName + '].[' + @TableName + '];'

	PRINT @ColumnName
	FETCH NEXT FROM db_cursor INTO @ColumnName

END

CLOSE db_cursor
DEALLOCATE db_cursor

SET @Query = @Query + 'DECLARE @ClusteredIndex VARCHAR(MAX) SET @ClusteredIndex = '''''
SET @Query = @Query + 'SELECT @ClusteredIndex = @ClusteredIndex + ColumnName + '' ASC,'' FROM #Results WHERE ColumnCount <> 1 ORDER BY ColumnCount DESC '
SET @Query = @Query + 'IF @ClusteredIndex <> '''' BEGIN '
SET @Query = @Query + 'SELECT ''CREATE CLUSTERED INDEX [IX_Clustered] ON [' + @SchemaName + '].[' + @TableName + '] ('' + LEFT(@ClusteredIndex, LEN(@ClusteredIndex)-1) + '')'' AS ClusteredIndex '
SET @Query = @Query + 'END ELSE BEGIN '
SET @Query = @Query + '	SELECT '''' AS ClusteredIndex '
SET @Query = @Query + 'END'

EXEC(@Query)