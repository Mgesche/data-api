SELECT	QUOTENAME(SCE.name) AS SchemaName, QUOTENAME(TAB.name) AS TableName,
		LIG.Nb_Lignes,
		CASE WHEN HASHCOL.object_id IS NOT NULL
			 THEN CASE WHEN HAS.Table_Name IS NOT NULL
			           THEN 'DROP INDEX [IX_HashColumn] ON ' + QUOTENAME(SCE.name) + '.' + QUOTENAME(TAB.name) + ';'
		        END + 'CREATE CLUSTERED INDEX [IX_HashColumn] ON ' + QUOTENAME(SCE.name) + '.' + QUOTENAME(TAB.name) + '(SYS_HashColumn ASC)'
		END AS SQLCommand

FROM sys.tables TAB

JOIN sys.schemas SCE
  ON SCE.schema_id = TAB.schema_id

LEFT JOIN (SELECT DISTINCT s.name AS Schema_Name, t.name AS Table_Name
FROM sys.tables t
INNER JOIN sys.indexes i ON t.object_id = i.object_id
INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE i.type <> 0
  AND i.type_desc LIKE 'CLUSTERED%'
) CLU
  ON CLU.Schema_Name = SCE.name
 AND CLU.Table_Name = TAB.name

LEFT JOIN (SELECT DISTINCT s.name AS Schema_Name, t.name AS Table_Name
FROM sys.tables t
INNER JOIN sys.indexes i ON t.object_id = i.object_id
INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE i.type <> 0
  AND i.name = 'IX_HashColumn'
) HAS
  ON HAS.Schema_Name = SCE.name
 AND HAS.Table_Name = TAB.name

LEFT JOIN (SELECT s.name AS Schema_Name, t.name AS Table_Name, MAX(PS.row_count) AS Nb_Lignes
FROM sys.tables t
JOIN sys.schemas s
  ON s.schema_id = t.schema_id
JOIN sys.dm_db_partition_stats AS PS
  ON PS.object_id = t.object_id
WHERE PS.index_id BETWEEN 0 AND 1
GROUP BY s.name, t.name) LIG
  ON SCE.name = LIG.Schema_Name
 AND TAB.name = LIG.Table_Name

LEFT JOIN (SELECT object_id
  FROM sys.columns
  WHERE name = 'SYS_HashColumn') HASHCOL
    ON HASHCOL.object_id = TAB.object_id

WHERE CLU.Table_Name IS NULL
  AND LIG.Nb_Lignes > 0