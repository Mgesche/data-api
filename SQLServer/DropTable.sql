IF EXISTS(SELECT * FROM [%DBName%].INFORMATION_SCHEMA.TABLES WHERE table_name = '%TableName%' AND table_schema = '%SchemaName%')
BEGIN
    DROP TABLE %SchemaName%.%TableName%;
END