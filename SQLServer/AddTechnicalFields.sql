DECLARE @DomainName VARCHAR(255)
DECLARE @DBName VARCHAR(255)
DECLARE @SchemaName VARCHAR(255)
DECLARE @TableName VARCHAR(255)

SET @DomainName = '%DomainName%'
SET @DBName = '%DBName%'
SET @SchemaName = '%SchemaName%'
SET @TableName = '%TableName%'

DECLARE @AppODSDatabase VARCHAR(64)

SET @DBName = REPLACE(REPLACE(@DBName, '[', ''), ']', '')
SET @SchemaName = REPLACE(REPLACE(@SchemaName, '[', ''), ']', '')
SET @TableName = REPLACE(REPLACE(@TableName, '[', ''), ']', '')

SELECT @AppODSDatabase = CASE WHEN @DomainName = 'ASSIST' THEN 'DQ_STG_Assist' ELSE @DomainName + '_ODS' END

/* BatchId */
IF NOT EXISTS(SELECT 1 
FROM %DBName%.INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TableName
  AND TABLE_SCHEMA = @SchemaName
 AND COLUMN_NAME = 'SYS_BatchId'
BEGIN
	ALTER TABLE %DBName%.%SchemaName%.%TableName% ADD [SYS_BatchId] [int] NULL
END

/* LastModified */
IF NOT EXISTS(SELECT 1 
FROM %DBName%.INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TableName
  AND TABLE_SCHEMA = @SchemaName
 AND COLUMN_NAME = 'SYS_LastModified'
BEGIN
	ALTER TABLE %DBName%.%SchemaName%.%TableName% ADD [SYS_LastModified] [datetime2] NULL
END

/* Ajout des HashColumn calculée pour stg */
IF @SchemaName LIKE '%_stg%'
BEGIN

    /* Calcul du script hash */
    DECLARE @HashCodeScript VARCHAR(MAX)
    EXEC [Model].[GetHashCodeScript] @DBName, @SchemaName, @TableName, @HashCodeScript OUTPUT

    SET @Query = N'ALTER TABLE '+@SchemaName+'.'+CASE WHEN LEFT(@TableName, 1) = '[' THEN @TableName ELSE '[' + @TableName + ']' END
    SET @Query = @Query + CHAR(13) + ' ADD SYS_HashColumn AS '+ @HashCodeScript + ' PERSISTED;'

    SET @QueryExec = 'EXEC '+@DBName+'.dbo.sp_executesql N'''+REPLACE(@Query, '''', '''''')+''''
    EXEC dbo.sp_executesql @QueryExec

END ELSE BEGIN

    /* HashColumn */
    EXECUTE [Model].[AddTechnicalFieldIfNecessary]
        @DBName = @DBName
        ,@SchemaName = @SchemaName
        ,@TableName = @TableName
        ,@FieldName = 'SYS_HashColumn'
        ,@FieldOptions = '[nvarchar](32) NULL'

END

/* Creation des index */
SET @Query =                     'CREATE TABLE #ClePK ('
SET @Query = @Query + CHAR(13) + '     [CleColumn] VARCHAR(255) ' + CASE WHEN @DomainName = 'HR' THEN 'COLLATE Latin1_General_CS_AS' ELSE '' END + ','
SET @Query = @Query + CHAR(13) + '     [CleOrder] INT '
SET @Query = @Query + CHAR(13) + ');'

SET @Query = @Query + CHAR(13) + 'INSERT INTO #ClePK'
SET @Query = @Query + CHAR(13) + 'SELECT KCU.COLUMN_NAME AS COLUMN_NAME, KCU.ORDINAL_POSITION'
SET @Query = @Query + CHAR(13) + 'FROM ['+@DBName+'].INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC'
SET @Query = @Query + CHAR(13) + 'JOIN ['+@DBName+'].INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU'
SET @Query = @Query + CHAR(13) + '  ON KCU.CONSTRAINT_SCHEMA = TC.CONSTRAINT_SCHEMA'
SET @Query = @Query + CHAR(13) + ' AND KCU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME'
SET @Query = @Query + CHAR(13) + ' AND KCU.TABLE_SCHEMA = TC.TABLE_SCHEMA'
SET @Query = @Query + CHAR(13) + ' AND KCU.TABLE_NAME = TC.TABLE_NAME'
SET @Query = @Query + CHAR(13) + 'WHERE TC.CONSTRAINT_TYPE = ''PRIMARY KEY'''
SET @Query = @Query + CHAR(13) + '  AND KCU.TABLE_NAME = ''' + @TableName + ''''
SET @Query = @Query + CHAR(13) + '  AND KCU.TABLE_SCHEMA = ''' + REPLACE(@SchemaName, '_stg', '') + ''''
SET @Query = @Query + CHAR(13) + 'ORDER BY KCU.ORDINAL_POSITION'

SET @Query = @Query + CHAR(13) + 'DECLARE @sqlPK VARCHAR(MAX)'
SET @Query = @Query + CHAR(13) + 'DECLARE @sqlPKIndex NVARCHAR(MAX)'
SET @Query = @Query + CHAR(13) + 'SET @sqlPK = '''''
SET @Query = @Query + CHAR(13) + 'SELECT @sqlPK = @sqlPK + ''['' + [CleColumn] + ''] ASC,'''
SET @Query = @Query + CHAR(13) + 'FROM #ClePK'
SET @Query = @Query + CHAR(13) + 'ORDER BY CleOrder'

SET @Query = @Query + CHAR(13) + 'SET @sqlPKIndex = ''CREATE NONCLUSTERED INDEX [IX_HashColumn] ON ['+@SchemaName+'].['+@TableName+']'''
SET @Query = @Query + CHAR(13) + 'SET @sqlPKIndex = @sqlPKIndex + ''('' + @sqlPK + ''SYS_HashColumn ASC)'';'

SET @Query = @Query + CHAR(13) + 'EXEC dbo.sp_executesql @sqlPKIndex;'

SET @QueryExec = 'EXEC '+@DBName+'.dbo.sp_executesql N'''+REPLACE(@Query, '''', '''''')+''''
EXEC dbo.sp_executesql @QueryExec