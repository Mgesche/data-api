DECLARE @AncienNbLines INT
DECLARE @NouveauNbLines INT
DECLARE @Action VARCHAR(255)

SELECT %SelectColumnAGG%
INTO [%SchemaName%].[%TableName_History%_Save]
FROM [%SchemaName%].[%TableName_History%]
GROUP BY %GroupByColumn%

SELECT @AncienNbLines = COUNT(*)
FROM [%SchemaName%].[%TableName_History%]

SELECT @NouveauNbLines = COUNT(*)
FROM [%SchemaName%].[%TableName_History%_Save]

/* On ne compresse la table temporelle qu'en cas d'explosion du nombre de ligne, ie on double les lignes */

IF @AncienNbLines > 5 * @NouveauNbLines
BEGIN

      ALTER TABLE [%SchemaName%].[%TableName%] SET ( SYSTEM_VERSIONING = OFF  )

      BEGIN TRY

            DECLARE @Query NVARCHAR(MAX)
            SET @Query = 'INSERT INTO [%SchemaName%].[%TableName_History%] (
                  %SelectColumn%
                  ) SELECT %SelectColumn%
            FROM [%SchemaName%].[%TableName_History%_Save]'
            EXEC(@Query)

            ALTER TABLE [%SchemaName%].[%TableName%] SET ( SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [%SchemaName%].[%TableName_History%] ) )

            SET @Action = 'Compress'

      END TRY
      BEGIN CATCH

            /* On est oublige de purger car sinon, overlapping dates... */
            DECLARE @QueryTruncate NVARCHAR(MAX)
            SET @QueryTruncate = 'TRUNCATE TABLE [%SchemaName%].[%TableName_History%]'
            EXEC(@QueryTruncate)

            ALTER TABLE [%SchemaName%].[%TableName%] SET ( SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [%SchemaName%].[%TableName_History%] ) )

            SET @Action = 'Truncate'

      END CATCH

END ELSE BEGIN

      SET @Action = 'None'

END

DROP TABLE [%SchemaName%].[%TableName_History%_Save]

SELECT '[%SchemaName%]' AS SchemaName, '[%TableName%]' AS TableName, @AncienNbLines AS AncienNbLines, @NouveauNbLines AS NouveauNbLines, @Action AS ActionExecuted