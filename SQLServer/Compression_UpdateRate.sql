/*
	DECLARE @AppName VARCHAR(255)
	DECLARE @EntityName VARCHAR(255)

	SET @AppName = 'FIN_ODS'
	SET @EntityName = 'Cost_Centre'

	SELECT *
	FROM (
	SELECT	CAST(DtLaunch AS DATE) AS DtLaunch,
			
			RANK() OVER(PARTITION BY BAT.SSISPackage ORDER BY BAT.DtLaunch DESC) AS RANK
	FROM Events.Event EVE
	JOIN biml.Batchs BAT
	  ON BAT.SYS_BatchId = EVE.SYS_BatchId
	WHERE AppName = @AppName
	  AND EntityName = @EntityName
	) EVE
	WHERE RANK = 1
	*/