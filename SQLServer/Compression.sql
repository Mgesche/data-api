/*

Meilleurs candidats :
* Log tables
* Audit tables
* Fact tables
* Reporting

Coté ODS :
* Rafraichissement en Daily / Monthly
* Truncate / Insert
* Historical table
* Staging tables

*/

CREATE TABLE #ToCompress_Base(
	object_id INT,
	Reason VARCHAR(255)
)

CREATE TABLE #ToCompress(
	object_id INT,
	Reason VARCHAR(255)
)

/* Temporal tables */
DECLARE @Has_TemporalTable INT

SELECT @Has_TemporalTable = CASE WHEN compatibility_level > 120 THEN 1 ELSE 0 END
FROM sys.databases
WHERE database_id = DB_ID();

IF @Has_TemporalTable = 1
BEGIN

	INSERT INTO #ToCompress_Base
	SELECT history_table_id, 'Temporal tables' AS Reason
	from sys.tables t
	WHERE history_table_id IS NOT NULL

END

/* Staging tables */
INSERT INTO #ToCompress_Base
SELECT OBJ.object_id, 'Staging tables' AS Reason
FROM sys.objects OBJ
JOIN sys.schemas SCE
  ON OBJ.schema_id = SCE.schema_id
WHERE SCE.name LIKE '%stg'

/* More than 50 000 lines */
INSERT INTO #ToCompress_Base
SELECT OBJ.object_id, 'More than 50 000 lines' AS Reason
FROM sys.objects OBJ
JOIN (SELECT t.object_id, MAX(PS.row_count) AS Nb_Lignes
FROM sys.tables t
JOIN sys.schemas s
  ON s.schema_id = t.schema_id
JOIN sys.dm_db_partition_stats AS PS
  ON PS.object_id = t.object_id
WHERE PS.index_id BETWEEN 0 AND 1
GROUP BY t.object_id) LIG
 ON LIG.object_id = OBJ.object_id
WHERE LIG.Nb_Lignes > 50000

/* Facts */
INSERT INTO #ToCompress_Base
SELECT OBJ.object_id, 'Facts' AS Reason
FROM sys.objects OBJ
WHERE name LIKE '%FACT%'

/* Remove duplicates */
INSERT INTO #ToCompress
SELECT object_id, MAX(Reason) AS Reason
FROM #ToCompress_Base
GROUP BY object_id

/* Export result */
SELECT OBJ.name AS TableName,
       i.name AS IndexName,
       p.data_compression_desc AS CurrentCompression,
	   LIG.Nb_Lignes,
	   TAI.TotalSpaceKB,
	   TAI.UsedSpaceKB,
	   TAI.UnusedSpaceKB,
	   COM.Reason,
	   --'ALTER TABLE [' + SCE.name +'].[' + OBJ.name +'] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = PAGE)' AS SQLCommand
	   'ALTER INDEX [' + i.name + '] ON [' + SCE.name +'].[' + OBJ.name +'] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = PAGE)' AS SQLCommand
FROM sys.objects OBJ
JOIN #ToCompress COM
  ON COM.object_id = OBJ.object_id
JOIN sys.schemas SCE
  ON OBJ.schema_id = SCE.schema_id
JOIN sys.indexes AS i
  ON OBJ.object_id = i.object_id
JOIN sys.partitions AS p
  ON i.object_id = p.object_id
 AND i.index_id = p.index_id
JOIN (SELECT t.object_id,
		SUM(a.total_pages) * 8 AS TotalSpaceKB,
		SUM(a.used_pages) * 8 AS UsedSpaceKB,
		(SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
FROM sys.tables t
INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE t.NAME NOT LIKE 'dt%' AND t.is_ms_shipped = 0 AND i.OBJECT_ID > 255
GROUP BY t.object_id) TAI
  ON TAI.object_id = OBJ.object_id
LEFT JOIN (SELECT t.object_id, MAX(PS.row_count) AS Nb_Lignes
FROM sys.tables t
JOIN sys.schemas s
  ON s.schema_id = t.schema_id
JOIN sys.dm_db_partition_stats AS PS
  ON PS.object_id = t.object_id
WHERE PS.index_id BETWEEN 0 AND 1
GROUP BY t.object_id) LIG
 ON LIG.object_id = OBJ.object_id
WHERE p.data_compression_desc <> 'PAGE'

DROP TABLE #ToCompress_Base
DROP TABLE #ToCompress


/*

SELECT APP.SchemaName, TRA.TargetEntityName
FROM ETLCore.biml.Transfer TRA
JOIN ETLCore.biml.Applications APP
  ON APP.AppName = TRA.TargetAppName
WHERE TRA.SourceAppName LIKE '%ODS%'

DECLARE @Frequency VARCHAR(255)
	DECLARE @TransferType VARCHAR(255)
	DECLARE @Compression_UpdateRate DECIMAL(18,6)
	DECLARE @IsCompressionAdvised INT

	SELECT	@Frequency = Frequency,
			@TransferType = TransferType,
			@Compression_UpdateRate = CASE	WHEN SourceAppName LIKE '%ODS%'
											THEN biml.Compression_UpdateRate(SourceAppName, SourceEntityName)
											ELSE biml.Compression_UpdateRate(TargetAppName, TargetEntityName)
									  END
	FROM biml.Transfer
	WHERE SourceAppName = @SourceAppName
	  AND SourceEntityName = @SourceEntityName
	  AND TargetAppName = @TargetAppName
	  AND TargetEntityName = @TargetEntityName

	SELECT @IsCompressionAdvised = CASE WHEN @Frequency IN ('Daily', 'Monthly')
										  OR @TransferType = 'Truncate/Insert'
										  OR @Compression_UpdateRate < 0.05
										THEN 1
										ELSE 0
								   END
	
	RETURN @IsCompressionAdvised

*/