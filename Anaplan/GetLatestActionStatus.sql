SELECT *
FROM ETLCore.ana.ErrorMessages
WHERE FunctionName <> 'Authenticate_UserPassword'
  AND AnaplanActionChain <> ''
  AND DtError > '%LastPropagationKey%'
ORDER BY DtError DESC