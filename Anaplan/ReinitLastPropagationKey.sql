UPDATE [ETLCore].[ana].[LastRun]
SET LastPropagationKey = NULL WHERE ActionChain+Action+CONVERT(VARCHAR, LastPropagationKey, 104) IN (
SELECT ActionChain+Action+CONVERT(VARCHAR, LastPropagationKey, 104) FROM (
SELECT *, RANK() OVER(PARTITION BY ActionChain, Action ORDER BY LastPropagationKey DESC) AS RANK
FROM [ETLCore].[ana].[LastRun]
WHERE ActionChain IN ('RADAR_I', 'GO_SO', 'ADH_RAD', 'RADAR_II', 'RADAR_III', 'RADAR_IV', 'Clear_FBT', 'HQ Lists', 'PS TRIAL', 'AR_CONT', 'PS TRIAL_12H', 'PS TRIAL_22H', 'GO_BE')
) RES
WHERE RANK = 1
)