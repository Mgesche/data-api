DECLARE @RuleName VARCHAR(255)
SET @RuleName = '%RuleName%'

DECLARE @Domain VARCHAR(255)
SET @Domain = '%Domain%'

DECLARE @Metier VARCHAR(255)
SET @Metier = '%Metier%'

DECLARE @SchemaName VARCHAR(255)
SET @SchemaName = '%SchemaName%'

DECLARE @TestDescription VARCHAR(255)
SET @TestDescription = '%TestDescription%'

DECLARE @RuleNameFormat VARCHAR(255)
SET @RuleNameFormat = '[%SchemaName%].[%RuleNameFormat%]'

DECLARE @MainEntityName VARCHAR(255)
SET @MainEntityName = '%MainEntityName%'

DECLARE @FunctionalArea VARCHAR(255)
SET @FunctionalArea = '%FunctionalArea%'

DECLARE @Severity VARCHAR(255)
SET @Severity = '%Severity%'

DECLARE @Author VARCHAR(255)
SET @Author = '%Author%'

DECLARE @TFS VARCHAR(255)
SET @TFS = '%TFS%'

IF NOT EXISTS(SELECT 1 FROM qc.Metier WHERE [Name] = @Metier)
BEGIN
	INSERT INTO qc.Metier (
		[Name]
	)
	SELECT @Metier
END

DECLARE @MetierId INT
SELECT @MetierId = [Id]
FROM qc.Metier
WHERE [Name] = @Metier

DECLARE @MainEntityId INT
SELECT @MainEntityId = [Id]
FROM qc.Entity
WHERE [Name] = @MainEntityName

DECLARE @SeverityId INT
SELECT @SeverityId = [Id]
FROM qc.Severity
WHERE [Name] = @Severity

DECLARE @LinkToTFS VARCHAR(500)
SET @LinkToTFS = (SELECT CASE WHEN @TFS = '' THEN 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries' ELSE 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries?id=' + @TFS END)

DECLARE @LinkToKB VARCHAR(500)
SET @LinkToKB = (SELECT CASE WHEN @TFS = '' THEN 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries' ELSE 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries?id=' + @TFS END)

DECLARE @LinkToSourceApplication VARCHAR(500)
SET @LinkToSourceApplication = (SELECT CASE WHEN @TFS = '' THEN 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries' ELSE 'https://tfs.ext.icrc.org/tfs/ICRCCollection/Data%20Quality%20Management/_queries?id=' + @TFS END)

IF NOT EXISTS (SELECT 1 FROM [qc].[Config] WHERE [RuleToExecute] = @RuleNameFormat)
BEGIN
    INSERT INTO [qc].[Config] (
        [MetierId]
      ,[Name]
      ,[Description]
      ,[RuleToExecute]
      ,[Grouping]
      ,[TypeId]
      ,[SeverityId]
      ,[MainEntityId]
      ,[IsConfidential]
      ,[Contact]
      ,[LinkToTFS]
      ,[LinkToKB]
      ,[LinkToSourceApplication]
    )
    SELECT @MetierId AS MetierId, @RuleName AS RuleName, @TestDescription AS [Description], @RuleNameFormat AS RuleToExecute,
           @FunctionalArea AS [Grouping], 1 AS [TypeId], @SeverityId AS [SeverityId], @MainEntityId AS [MainEntityId], 0 AS [IsConfidential]
          ,@Author AS [Contact]
          ,@LinkToTFS AS [LinkToTFS],
           @LinkToKB AS LinkToKB,
           @LinkToSourceApplication AS LinkToSourceApplication
           

END

UPDATE [qc].[Config] SET [MetierId] = @MetierId
WHERE [RuleToExecute] = @RuleNameFormat
  AND [MetierId] <> @MetierId

UPDATE [qc].[Config] SET [Description] = @TestDescription
WHERE [RuleToExecute] = @RuleNameFormat
  AND [Description] <> @TestDescription

UPDATE [qc].[Config] SET [RuleToExecute] = @RuleNameFormat
WHERE [RuleToExecute] = @RuleNameFormat
  AND [RuleToExecute] <> @RuleNameFormat

UPDATE [qc].[Config] SET [Grouping] = @FunctionalArea
WHERE [RuleToExecute] = @RuleNameFormat
  AND [Grouping] <> @FunctionalArea

UPDATE [qc].[Config] SET [SeverityId] = @SeverityId
WHERE [RuleToExecute] = @RuleNameFormat
  AND [SeverityId] <> @SeverityId

UPDATE [qc].[Config] SET [MainEntityId] = @MainEntityId
WHERE [RuleToExecute] = @RuleNameFormat
  AND [MainEntityId] <> @MainEntityId

UPDATE [qc].[Config] SET [Contact] = @Author
WHERE [RuleToExecute] = @RuleNameFormat
  AND [Contact] <> @Author

UPDATE [qc].[Config] SET [LinkToTFS] = @LinkToTFS
WHERE [RuleToExecute] = @RuleNameFormat
  AND [LinkToTFS] <> @LinkToTFS

UPDATE [qc].[Config] SET [LinkToKB] = @LinkToKB
WHERE [RuleToExecute] = @RuleNameFormat
  AND [LinkToKB] <> @LinkToKB

UPDATE [qc].[Config] SET [LinkToSourceApplication] = @LinkToSourceApplication
WHERE [RuleToExecute] = @RuleNameFormat
  AND [LinkToSourceApplication] <> @LinkToSourceApplication