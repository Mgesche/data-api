SELECT COUNT(*) AS TotalErrors, COUNT(*) - COALESCE(SUM(CASE WHEN ResolveDate IS NOT NULL THEN 1 ELSE 0 END), 0) AS PendingErrors, COALESCE(SUM(CASE WHEN ResolveDate IS NOT NULL THEN 1 ELSE 0 END), 0) AS CorrectedErrors
FROM [DQS].[ass].[DqResults]
WHERE RuleName = '%RuleNameFormat%'