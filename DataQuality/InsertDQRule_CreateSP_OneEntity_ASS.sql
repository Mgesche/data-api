CREATE FUNCTION [%SchemaName%].[%RuleNameFormat%]()
RETURNS TABLE
AS RETURN (

/* %RuleName% */
SELECT	--	DISTINCT
		COALESCE(Substring(%GOCodeDef%, 8, 6), '<?>') AS Dim_Metier
	,	%ReferenceYearDef%            				  AS Dim_ReferenceYear
	,	COALESCE(PMT.DelegationCode, '<?>')			  AS Dim_Delegation
	,	COALESCE(Substring(%GOCodeDef%, 5, 3), '<?>') AS Dim_Target_Population
	,	COALESCE(%GOCodeDef%, '<?>')                  AS Dim_Objective
	,	%FunctionalKey%	                              AS functionalKey
	,	%CurrentValue%	                              AS currentValue
	,	%ExpectedValue%                               AS expectedValue
	,	%Orga%                                        AS Orga
	,	%Description%                                 AS Description
	,	1											  AS ErrorFlag
	,	1											  AS NbErrors

FROM (%MainQuery%) RES

LEFT JOIN ( SELECT PMT.*, DEL.Name AS DelegationName FROM (SELECT *,
	RANK() OVER(PARTITION BY Code, Year ORDER BY CASE Status WHEN '' THEN 1 WHEN 'REJECTED' THEN 2 WHEN 'CREATED' THEN 3 WHEN 'DRAFT' THEN 4 WHEN 'SUBMITTED' THEN 5 WHEN 'SHARED' THEN 6 WHEN 'SUBMITTED' THEN 7 WHEN 'ADOPTED' THEN 8 END DESC, CASE PeriodCode WHEN '' THEN 1 WHEN 'Q1' THEN 2 WHEN 'Q2' THEN 3 WHEN 'R1' THEN 4 WHEN 'R2' THEN 5 WHEN 'P0' THEN 6 END DESC, ModifiedDate DESC) AS Rank
FROM ASS_ODS.pmt.GOExport
WHERE IsDeleted = 0) PMT
LEFT JOIN FIN_ODS.refr.ICRC_Delegation DEL
  ON DEL.Code = PMT.DelegationCode
 AND DEL.Version = (SELECT MAX(Version) FROM FIN_ODS.refr.ICRC_Delegation)
WHERE Rank = 1) PMT
   ON PMT.Code = %GOCodeDef%
  AND PMT.Year = %ReferenceYearDef%

WHERE (%ReferenceYearDef% > ASS_ODS.rpt.YearLimit()-1 OR %ReferenceYearDef% IS NULL)
  %WhereClause%

UNION

SELECT	--	DISTINCT
		COALESCE(Substring(%GOCodeDef%, 8, 6), '<?>') AS Dim_Metier
	,	%ReferenceYearDef%            				  AS Dim_ReferenceYear
	,	COALESCE(PMT.DelegationCode, '<?>')			  AS Dim_Delegation
	,	COALESCE(Substring(%GOCodeDef%, 5, 3), '<?>') AS Dim_Target_Population
	,	COALESCE(%GOCodeDef%, '<?>')                  AS Dim_Objective
	,	NULL			                              AS functionalKey
	,	NULL	                              		  AS currentValue
	,	NULL                               		  	  AS expectedValue
	,	%Orga%                                        AS Orga
	,	NULL                                       	  AS Description
	,	0											  AS ErrorFlag
	,	COUNT(*)									  AS NbErrors

FROM (%MainQuery%) RES

LEFT JOIN ( SELECT PMT.*, DEL.Name AS DelegationName FROM (SELECT *,
	RANK() OVER(PARTITION BY Code, Year ORDER BY CASE Status WHEN '' THEN 1 WHEN 'REJECTED' THEN 2 WHEN 'CREATED' THEN 3 WHEN 'DRAFT' THEN 4 WHEN 'SUBMITTED' THEN 5 WHEN 'SHARED' THEN 6 WHEN 'SUBMITTED' THEN 7 WHEN 'ADOPTED' THEN 8 END DESC, CASE PeriodCode WHEN '' THEN 1 WHEN 'Q1' THEN 2 WHEN 'Q2' THEN 3 WHEN 'R1' THEN 4 WHEN 'R2' THEN 5 WHEN 'P0' THEN 6 END DESC, ModifiedDate DESC) AS Rank
FROM ASS_ODS.pmt.GOExport
WHERE IsDeleted = 0) PMT
LEFT JOIN FIN_ODS.refr.ICRC_Delegation DEL
  ON DEL.Code = PMT.DelegationCode
 AND DEL.Version = (SELECT MAX(Version) FROM FIN_ODS.refr.ICRC_Delegation)
WHERE Rank = 1) PMT
   ON PMT.Code = %GOCodeDef%
  AND PMT.Year = %ReferenceYearDef%

GROUP BY COALESCE(Substring(%GOCodeDef%, 8, 6), '<?>')
	,	%ReferenceYearDef%
	,	COALESCE(PMT.DelegationCode, '<?>')
	,	COALESCE(Substring(%GOCodeDef%, 5, 3), '<?>')
	,	COALESCE(%GOCodeDef%, '<?>')
	,	%Orga%

)