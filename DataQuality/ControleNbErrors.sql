SELECT testInstanceId, MIN(SysStartTime) AS SysStartTime, COUNT(*) AS NbErrors
FROM rpt.ResultDetail FOR SYSTEM_TIME ALL
WHERE testId = 'For the repeated distributions, repeated individuals cannot be 0'
GROUP BY testInstanceId
ORDER BY testInstanceId DESC