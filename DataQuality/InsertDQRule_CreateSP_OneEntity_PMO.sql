CREATE FUNCTION [%SchemaName%].[%RuleNameFormat%]()
RETURNS TABLE
AS RETURN (

/* %RuleName% */
SELECT	--	DISTINCT
		%ReferenceYearDef%            				  AS Dim_ReferenceYear
	,	%FunctionalKey%	                              AS functionalKey
	,	%CurrentValue%	                              AS currentValue
	,	%ExpectedValue%                               AS expectedValue
	,	%Orga%                                        AS Orga
	,	%Description%                                 AS Description

FROM (%MainQuery%) RES

WHERE (%ReferenceYearDef% > ASS_ODS.rpt.YearLimit()-1 OR %ReferenceYearDef% IS NULL)
  %WhereClause%

)