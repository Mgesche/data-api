DECLARE @RuleName VARCHAR(255)
SET @RuleName = '%RuleName%'

DECLARE @Domain VARCHAR(255)
SET @Domain = '%Domain%'

DECLARE @FunctionalArea VARCHAR(255)
SET @FunctionalArea = '%FunctionalArea%'

DECLARE @TestDescription VARCHAR(255)
SET @TestDescription = '%TestDescription%'

DECLARE @RuleNameFormat VARCHAR(255)
SET @RuleNameFormat = '%RuleNameFormat%'

DECLARE @SP VARCHAR(255)
SET @SP = '%SP%'

DECLARE @Destination VARCHAR(255)
SET @Destination = '%Destination%'

DECLARE @SchemaName VARCHAR(255)
SET @SchemaName = '%SchemaName%'

IF NOT EXISTS (SELECT 1 FROM [dbo].[Config] WHERE RuleName = '[' + @SchemaName + '].[' + @RuleNameFormat + ']')
BEGIN
    INSERT INTO [dbo].[Config] (
        [RuleName]
        ,[Domain]
        ,[FunctionalArea]
        ,[Description]
        ,[SP]
        ,[Destination]
    )
    SELECT '[' + @SchemaName + '].[' + @RuleNameFormat + ']' AS RuleName, @Domain AS Domain, @FunctionalArea AS FunctionalArea, @TestDescription AS Description, @SP AS SP, @Destination AS Destination
END

UPDATE [dbo].[Config] SET [Domain] = @Domain
WHERE RuleName = @RuleNameFormat
  AND [Domain] <> @Domain

UPDATE [dbo].[Config] SET [FunctionalArea] = @FunctionalArea
WHERE RuleName = @RuleNameFormat
  AND [FunctionalArea] <> @FunctionalArea

UPDATE [dbo].[Config] SET [Description] = @TestDescription
WHERE RuleName = @RuleNameFormat
  AND [Description] <> @TestDescription

UPDATE [dbo].[Config] SET [SP] = @SP
WHERE RuleName = @RuleNameFormat
  AND [SP] <> @SP

UPDATE [dbo].[Config] SET [Destination] = @Destination
WHERE RuleName = @RuleNameFormat
  AND [Destination] <> @Destination