SET NOCOUNT ON
SELECT SUM([Nb Errors]) AS [Nb Errors], SUM([Nb Lines]) AS [Nb Lines], LEFT(CAST(100.0*SUM([Nb Errors])/SUM([Nb Lines]) AS VARCHAR), 4) + '%' AS FailureRate, MAX(Threshold) AS Threshold  FROM (
SELECT *, RANK() OVER(PARTITION BY testId ORDER BY testInstanceId DESC) AS RANK
FROM [qc].[Run]
JOIN [qc].[Config] CON
  ON RUN.ConfigId = CON.Id
WHERE CON.RuleToExecute = '%RuleNameFormat%'
) RES
WHERE RANK = 1