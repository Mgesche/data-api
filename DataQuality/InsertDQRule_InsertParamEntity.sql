IF NOT EXISTS(SELECT 1 FROM qc.Entity WHERE [Name] = '%EntityName%')
BEGIN
	INSERT INTO qc.Entity (
		[Name],
		[SourceApplication],
		[DataSetGatherer]
	)
	SELECT '%EntityName%', '%Domain%', '[%SchemaName%].[%EntityCountFormat%]()'
END